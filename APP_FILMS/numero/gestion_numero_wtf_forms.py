"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    numeroPorte_genre_regexp = "[0-9]"
    numeroPorte_genre_wtf = StringField("Clavioter le numero de porte ", validators=[Length(min=1, max=2, message="min 1 max 2"),
                                                                   Regexp(numeroPorte_genre_regexp,
                                                                          message="Clavioter un chiffres !! pas de caractères " 
                                                                                  "Exemple: 21"
                                                                            )
                                                                   ])

    submit = SubmitField("Enregistrer loyer")

class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    numeroPorte_genre_update_regexp = "[0-9]"
    numeroPorte_genre_update_wtf = StringField("Clavioter le numero de porte ", validators=[Length(min=1, max=2, message="min 1 max 2"),
                                                                          Regexp(numeroPorte_genre_update_regexp,
                                                                                 message="Clavioter un chiffres !! pas de caractères " 
                                                                                         "Exemple: 21"
                                                                                 )
                                                                          ])

    submit = SubmitField("Enregistrer loyer")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_genre_delete_wtf = StringField("Effacer le numero de porte")
    submit_btn_del = SubmitField("Effacer le numero de porte")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
