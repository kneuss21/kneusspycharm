-- OM 2021.02.17
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUETES MYSQL
-- Database: kneuss_david_info1c

-- Détection si une autre base de donnée du même nom existe ok ok

DROP DATABASE IF EXISTS kneuss_david_info1c;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS kneuss_david_info1c;

-- Utilisation de cette base de donnée

USE kneuss_david_info1c;
-- --------------------------------------------------------
-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 02 Juin 2021 à 12:33
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `kneuss_david_info1c`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_locataire`
--

CREATE TABLE `t_locataire` (
  `id_locataire` int(11) NOT NULL,
  `nom` text NOT NULL,
  `prenom` text,
  `sexe` text,
  `dateDeNaissance` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_locataire`
--

INSERT INTO `t_locataire` (`id_locataire`, `nom`, `prenom`, `sexe`, `dateDeNaissance`) VALUES
(1, 'david', 'Kneuss', 'homme', '1998-11-21'),
(2, 'Emeline', 'luc', 'Femme', '2001-07-10'),
(5, 'Narr', 'Alexandre', 'Homme', '2019-12-17'),
(13, 'luc', 'troup', 'homme', '1997-12-21'),
(16, 'nikolaj', 'tur', 'femme', '1997-12-12');

-- --------------------------------------------------------

--
-- Structure de la table `t_locavoirnumero`
--

CREATE TABLE `t_locavoirnumero` (
  `ID_locAvoirNumero` int(11) NOT NULL,
  `FK_locataire` int(11) NOT NULL,
  `FK_numero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_locavoirnumero`
--

INSERT INTO `t_locavoirnumero` (`ID_locAvoirNumero`, `FK_locataire`, `FK_numero`) VALUES
(7, 1, 5);

-- --------------------------------------------------------

--
-- Structure de la table `t_locverserloyer`
--

CREATE TABLE `t_locverserloyer` (
  `ID_locVerserLoye` int(11) NOT NULL,
  `FK_loyer` int(11) NOT NULL,
  `FK_locataire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_locverserloyer`
--

INSERT INTO `t_locverserloyer` (`ID_locVerserLoye`, `FK_loyer`, `FK_locataire`) VALUES
(11, 9, 5),
(12, 3, 5),
(13, 1, 1),
(17, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `t_loyer`
--

CREATE TABLE `t_loyer` (
  `ID_loyer` int(11) NOT NULL,
  `loyerNet` double DEFAULT NULL,
  `loyerCharge` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_loyer`
--

INSERT INTO `t_loyer` (`ID_loyer`, `loyerNet`, `loyerCharge`) VALUES
(1, 1233, 101),
(2, 2000, 250),
(3, 124, 4),
(4, 166616, NULL),
(5, 123, NULL),
(6, 1212, 2110),
(9, 1666, 765),
(10, 4000, 200);

-- --------------------------------------------------------

--
-- Structure de la table `t_numero`
--

CREATE TABLE `t_numero` (
  `ID_numero` int(11) NOT NULL,
  `numeroPorte` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_numero`
--

INSERT INTO `t_numero` (`ID_numero`, `numeroPorte`) VALUES
(1, 3),
(5, 12),
(6, 20),
(7, 31);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_locataire`
--
ALTER TABLE `t_locataire`
  ADD PRIMARY KEY (`id_locataire`);

--
-- Index pour la table `t_locavoirnumero`
--
ALTER TABLE `t_locavoirnumero`
  ADD PRIMARY KEY (`ID_locAvoirNumero`),
  ADD KEY `FK_locataire` (`FK_locataire`),
  ADD KEY `FK_numero` (`FK_numero`);

--
-- Index pour la table `t_locverserloyer`
--
ALTER TABLE `t_locverserloyer`
  ADD PRIMARY KEY (`ID_locVerserLoye`),
  ADD KEY `FK_loyer` (`FK_loyer`),
  ADD KEY `FK_locataire` (`FK_locataire`);

--
-- Index pour la table `t_loyer`
--
ALTER TABLE `t_loyer`
  ADD PRIMARY KEY (`ID_loyer`);

--
-- Index pour la table `t_numero`
--
ALTER TABLE `t_numero`
  ADD PRIMARY KEY (`ID_numero`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_locataire`
--
ALTER TABLE `t_locataire`
  MODIFY `id_locataire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `t_locavoirnumero`
--
ALTER TABLE `t_locavoirnumero`
  MODIFY `ID_locAvoirNumero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `t_locverserloyer`
--
ALTER TABLE `t_locverserloyer`
  MODIFY `ID_locVerserLoye` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `t_loyer`
--
ALTER TABLE `t_loyer`
  MODIFY `ID_loyer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `t_numero`
--
ALTER TABLE `t_numero`
  MODIFY `ID_numero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_locavoirnumero`
--
ALTER TABLE `t_locavoirnumero`
  ADD CONSTRAINT `t_locavoirnumero_ibfk_1` FOREIGN KEY (`FK_locataire`) REFERENCES `t_locataire` (`id_locataire`),
  ADD CONSTRAINT `t_locavoirnumero_ibfk_2` FOREIGN KEY (`FK_numero`) REFERENCES `t_numero` (`ID_numero`);

--
-- Contraintes pour la table `t_locverserloyer`
--
ALTER TABLE `t_locverserloyer`
  ADD CONSTRAINT `t_locverserloyer_ibfk_1` FOREIGN KEY (`FK_locataire`) REFERENCES `t_locataire` (`id_locataire`),
  ADD CONSTRAINT `t_locverserloyer_ibfk_2` FOREIGN KEY (`FK_loyer`) REFERENCES `t_loyer` (`ID_loyer`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
