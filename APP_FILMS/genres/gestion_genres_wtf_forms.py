"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms import DateField
from wtforms import SelectField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_genre_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_genre_wtf = StringField("Nouveau Nom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(nom_genre_regexp,
                                                                          message="Pas de chiffres, de caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait union")
                                                                   ])
    prenom_genre_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    prenom_genre_wtf = StringField("Nouveau Prenom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                 Regexp(prenom_genre_regexp,
                                                                        message="Pas de chiffres, de caractères "
                                                                                "spéciaux, "
                                                                                "d'espace à double, de double "
                                                                                "apostrophe, de double trait union")
                                                                 ])
    sexe_genre_wtf = SelectField('votre sexe',
                                 choices=[('homme','Je suis un homme'),('femme','Je suis une femme')],
                                 validate_choice=True)

    #sexe_genre_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    #sexe_genre_wtf = StringField("Nouveau Sexe (homme ou femme) ", validators=[Length(min=3, max=6, message="min 3 max 6"),
                                                                 #Regexp(sexe_genre_regexp,
                                                                        #message="Pas de chiffres, de caractères "
                                                                                #"spéciaux, "
                                                                                #"d'espace à double, de double "
                                                                                #"apostrophe, de double trait union")
                                                                 #])

    dateDeNaissance_genre_regexp = ""
    dateDeNaissance_genre_wtf = DateField('Nouvelle date (21/11/1312)', format='%d/%m/%Y')

    submit = SubmitField("Enregistrer Personne")


class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    nom_genre_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    nom_genre_update_wtf = StringField("Clavioter le nom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                          Regexp(nom_genre_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    prenom_genre_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    prenom_genre_update_wtf = StringField("Nouveau Prenom ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                 Regexp(prenom_genre_update_regexp,
                                                                        message="Pas de chiffres, de caractères "
                                                                                "spéciaux, "
                                                                                "d'espace à double, de double "
                                                                                "apostrophe, de double trait union")
                                                                 ])
    sexe_genre_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"

    sexe_genre_update_wtf = SelectField('votre sexe',
                                 choices=[('homme','Je suis un homme'),('femme','Je suis une femme')],
                                 validate_choice=True)
    #sexe_genre_update_wtf = StringField("Nouveau Sexe (homme ou femme) ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                 #Regexp(sexe_genre_update_regexp,
                                                                        #message="Pas de chiffres, de caractères "
                                                                                #"spéciaux, "
                                                                                #"d'espace à double, de double "
                                                                                #"apostrophe, de double trait union")
                                                                 #])
    dateDeNaissance_genre_update_regexp = ""
    dateDeNaissance_genre_update_wtf = DateField('Nouvelle date (21/11/1312)', format='%d/%m/%Y')
    submit = SubmitField("Update")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_genre_delete_wtf = StringField("Effacer cette Personne")
    submit_btn_del = SubmitField("Effacer la  Personne")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
