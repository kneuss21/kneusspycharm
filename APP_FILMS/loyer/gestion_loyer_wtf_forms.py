"""
    Fichier : gestion_genres_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""

from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterGenres(FlaskForm):
    """
        Dans le formulaire "genres_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    loyerNet_genre_regexp = "[0-9]"
    loyerNet_genre_wtf = StringField("Clavioter le loyer ", validators=[Length(min=1, max=8, message="min 1 max 8"),
                                                                   Regexp(loyerNet_genre_regexp,
                                                                          message="Clavioter un chiffres !! pas de caractères " 
                                                                                  "Exemple: 1912"
                                                                            )
                                                                   ])
    loyerCharge_genre_regexp = "[0-9]"
    loyerCharge_genre_wtf = StringField("Clavioter la charge ", validators=[Length(min=1, max=8, message="min 1 max 8"),
                                                                   Regexp(loyerCharge_genre_regexp,
                                                                          message="Clavioter un chiffres !! pas de caractères " 
                                                                                  "Exemple: 1912"
                                                                            )
                                                                   ])
    submit = SubmitField("Enregistrer loyer")


class FormWTFUpdateGenre(FlaskForm):
    """
        Dans le formulaire "genre_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    loyerNet_genre_update_regexp = "[0-9]"
    loyerNet_genre_update_wtf = StringField("Clavioter le loyer ", validators=[Length(min=1, max=8, message="min 1 max 8"),
                                                                          Regexp(loyerNet_genre_update_regexp,
                                                                                 message="Clavioter un chiffres !! pas de caractères " 
                                                                                         "Exemple: 1912"
                                                                                 )
                                                                          ])
    loyerCharge_genre_update_regexp = "[0-9]"
    loyerCharge_genre_update_wtf = StringField("Clavioter la charge ", validators=[Length(min=1, max=8, message="min 1 max 8"),
                                                                          Regexp(loyerCharge_genre_update_regexp,
                                                                                 message="Clavioter un chiffres !! pas de caractères "
                                                                                         "Exemple: 1912"
                                                                                 )
                                                                          ])
    submit = SubmitField("Update loyer")


class FormWTFDeleteGenre(FlaskForm):
    """
        Dans le formulaire "genre_delete_wtf.html"

        nom_genre_delete_wtf : Champ qui reçoit la valeur du genre, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "genre".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_genre".
    """
    nom_genre_delete_wtf = StringField("Effacer le loyer")
    submit_btn_del = SubmitField("Effacer loyer")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
