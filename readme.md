Manuel d'utilisation de la base de donnée en ligne pour le module 104

Marche à suivre :

1) En premier, installer python, PyChamr ainsi que Uwamp (voici des liens : https://www.python.org/ftp/python/3.9.5/python-3.9.5-amd64.exe  https://www.uwamp.com/fr/file/archive/UwAmp_3.0.2.exe  https://www.jetbrains.com/fr-fr/pycharm/download/download-thanks.html?platform=windows )

2) Aller sur le Gitlab du projet ou copier le lien ci-dessous : ( https://gitlab.com/kneuss21/kneusspycharm )

3) Lancer PyCharm puis créer un nouveau projet, cliquer sur le bouton Get from VCS. Puis copier le lien mit plus haut. Séléctionner un emplacement pour le projet (il faut le renommer, et éviter de le mettre sur le bureau ou alors dans un dossier avec des choses à l'inérieur)

4) Une fois ceci fait, lancer votre serveur Uwmap.

5) Par la suite, aller dans le projet, dans le dossier "zzzdemos" puis lancer en faisent un clique droit dessus, "1_ImportationDumpSql.py"

6) Si vous avez aucune erreur, alors lancez "1_run_server_flask.py". Puis cliquer sur le lien en bleu qui c'est afficher dans la console

7) Normalement vous arrivez sur une page de navigateur web, vous pouvez alors commencer à intéragire avec la base de donnée

8) En utilisant les différents boutons ainsi que les liens du menu de navigation vous pourrez vous baladez entre les différentes pages.

Si vous rencontrez des problèmes lors de l'utilisations du projet, merci d'utiliser mon adresse email pour me contacter :
DavidKneuss@hotmail.ch